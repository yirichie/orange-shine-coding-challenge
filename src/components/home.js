import React from "react";

import "../css/home.css";

const Home = () => {
  return (
    <div className="home-container">
      <img src={require("../assets/banner.png")} alt="" />
    </div>
  );
};

export default Home;
