import React from "react";
import { Link, useRouteMatch } from "react-router-dom";

import "../css/productDetails.css";

const ProductDetails = props => {
  let match = useRouteMatch();
  const [dress, setDress] = React.useState();

  React.useEffect(() => {
    const activeDress = props.dresses.filter(
      dress => dress.id === match.params.id
    )[0];
    setDress(activeDress);
  }, [props.dresses, match.params.id]);

  if (!dress) return null;

  const renderColors = () => {
    return dress.colors.map(color => <span key={color}>{color}</span>);
  };

  return (
    <div className="product-details-container">
      <div className="product-details-image-container">
        <img src={dress.image} alt={dress.description} />
      </div>
      <div className="product-details-info-container">
        <span className="product-brand">{dress.brand_name}</span>
        <span className="product-name">{dress.name}</span>
        <span className="product-description-text">Description</span>
        <span className="product-description">{dress.description}</span>
        <span className="price-text">Price</span>
        <span className="product-price">{`$${dress.price}`}</span>
        <span className="color-text">Colors</span>
        <div className="color-list">{renderColors()}</div>
        <Link to="/products" className="prev-container">
          <button>PREV</button>
        </Link>
      </div>
    </div>
  );
};

export default ProductDetails;
