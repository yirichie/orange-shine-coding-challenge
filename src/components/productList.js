import React from "react";
import { Link, useRouteMatch } from "react-router-dom";

import "../css/productList.css";

const ProductList = props => {
  let match = useRouteMatch();

  const renderDresses = () => {
    return props.dresses.map(dress => {
      return (
        <Link
          to={`${match.url}/${dress.id}`}
          className="product-container"
          key={dress.id}
        >
          <img src={dress.image} alt="" />
          <div className="product-info">
            <span className="brand-name">{dress.brand_name}</span>
            <span className="dress-price">{`$${dress.price}`}</span>
          </div>
        </Link>
      );
    });
  };

  return (
    <div className="product-list-container">
      <div className="dresses-container">{renderDresses()}</div>
    </div>
  );
};

export default ProductList;
