import React from "react";

import "../css/about.css";

const About = () => {
  return (
    <div className="about-container">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus
        augue quis odio tempus, nec lobortis ligula aliquet. Phasellus sit amet
        vehicula libero, mollis venenatis sapien. Proin vel cursus lacus, ut
        maximus felis. Nullam ligula dolor, posuere eu maximus hendrerit, porta
        in ante. Nam porttitor lorem eget ligula viverra, eget porta mi porta.
        Etiam sodales a massa non condimentum. Sed accumsan neque sed lorem
        ultrices, sed imperdiet tortor sagittis. Maecenas suscipit vulputate
        tortor vitae mattis. Etiam et suscipit erat. In vulputate metus rutrum
        risus blandit, a commodo felis porttitor. Donec a porta magna. Curabitur
        non bibendum mauris. Vestibulum ante ipsum primis in faucibus orci
        luctus et ultrices posuere cubilia Curae;
      </p>
      <p>
        Curabitur augue lorem, pretium in sollicitudin ac, venenatis ut metus.
        Aenean dignissim turpis eget enim fermentum aliquam. Suspendisse
        placerat, dui a condimentum laoreet, eros magna vulputate ipsum, vitae
        interdum sem arcu nec enim. Donec auctor laoreet risus quis mattis.
        Aliquam et condimentum magna. Donec eget arcu semper, vestibulum dolor
        sed, euismod tortor. Donec pulvinar ipsum aliquet lacus ultricies
        rhoncus. Praesent malesuada ante sit amet elit aliquam auctor.
        Pellentesque eleifend aliquam iaculis. Integer tincidunt eleifend justo,
        faucibus rutrum nibh semper non. Praesent sed mauris vel ex tincidunt
        tincidunt id ut eros. Sed id mollis nunc. Aliquam consequat mauris a
        arcu aliquam, quis ornare massa tempus. Sed dapibus laoreet ultricies.
      </p>
      <p>
        Integer posuere varius ante sed vehicula. Nulla id pellentesque ex. Nam
        quis tincidunt elit. Nam placerat ultrices ante egestas sodales. Nulla
        facilisi. Nam sed odio tortor. Aenean eu arcu ac lacus tempor lacinia
        nec non dui. Nam congue turpis diam. Sed congue volutpat nunc in
        fermentum.
      </p>
      <p>
        Aenean molestie imperdiet massa, ultrices ultrices neque dignissim quis.
        Morbi ac ligula sed felis bibendum blandit. Duis convallis elit eu
        rhoncus feugiat. Suspendisse a enim sed mi elementum laoreet sit amet
        quis tortor. Sed mollis tellus nec magna auctor eleifend. Cras egestas
        dui quis libero suscipit rhoncus. Nullam placerat libero semper, finibus
        magna at, dapibus erat. Curabitur efficitur consequat sem, eu sodales
        ligula finibus quis.
      </p>
      <p>
        Cras sit amet iaculis purus, et ultrices eros. Aliquam cursus eu enim
        eget maximus. Etiam elementum, tellus non fringilla sollicitudin, arcu
        enim ultricies mauris, vel pellentesque est risus ut est. Suspendisse
        potenti. Duis feugiat nulla at nibh efficitur interdum. Pellentesque
        habitant morbi tristique senectus et netus et malesuada fames ac turpis
        egestas. Morbi leo quam, ultrices et ultrices vel, ultrices id est.
        Nullam congue turpis orci, vel bibendum neque laoreet vel. Praesent
        lacinia lacinia nunc, vel consectetur quam lobortis nec. Proin auctor
        nisl ultrices suscipit efficitur.
      </p>
    </div>
  );
};

export default About;
