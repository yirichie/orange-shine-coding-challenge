import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from "./components/home";
import About from "./components/about";
import ProductList from "./components/productList";
import ProductDetails from "./components/productDetails";

import "./App.css";

const App = () => {
  const [dresses, setDresses] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    import("./productList.json").then(res => {
      setLoading(false);
      setDresses(res.default);
    });
  }, []);

  if (loading) return <div>Loading...</div>;

  return (
    <Router>
      <div className="App">
        <div className="nav-container">
          <Link to="/">HOME</Link>
          <Link to="/products">DRESSES</Link>
          <Link to="/about">ABOUT</Link>
        </div>
        <Switch>
          <Route path="/products/:id">
            <ProductDetails dresses={dresses} />
          </Route>
          <Route path="/products">
            <ProductList dresses={dresses} />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
